# Define here the models for your scraped items
#
# See documentation in=
# https=//docs.scrapy.org/en/latest/topics/items.html

import scrapy


class PtkBayernItem(scrapy.Item):
    salutation = scrapy.Field()
    services = scrapy.Field()
    email = scrapy.Field()
    company = scrapy.Field()
    geoCoordinates = scrapy.Field()
    homepage = scrapy.Field()
    country = scrapy.Field()
    location = scrapy.Field()
    patient_group = scrapy.Field()
    postal_code = scrapy.Field()
    score = scrapy.Field()
    last_name = scrapy.Field()
    street = scrapy.Field()
    first_name = scrapy.Field()
    fax = scrapy.Field()
    telephone = scrapy.Field()
    url = scrapy.Field()
    billing_basis = scrapy.Field()


'''
"anrede",
"dienstleistungen",
"email",
"firma",
"geoCoordinates",
"homepage",
"land",
"ort",
"patientengruppe",
"plz",
"score",
"sname",
"strasse",
"svorname",
"telefax",
"telefon",
"url",
"verrechnungsbasis"


    var hia_sURL = location.protocol + "//" + location.host + vcms_adressdb_pfad +
    "ptk_search_psychotherapeuten?OpenAgent&plz=" + s_PLZ + "&ort=" + s_ORT + "&umkreis=" + s_UMKREIS
    + "&name=" + s_NAME + "&pgruppe=" + s_PGRUPPE + "&versicherung=" + s_VERSICHERUNG
    + "&wleistungen=" + s_WLEISTUNGEN + "&verfahren=" + s_VERFAHREN
    + "&weitereverfahren=" + s_WEITEREVERFAHREN + "&weitereverfahren_wissen=" + s_WEITEREVERFAHREN_WISSEN
    + "&geschlecht=" + s_GESCHLECHT + "&oeffentlich=" + s_OEFFENTLICH + "&barrierefrei=" + s_BARRIEREFREI
    + "&corona=" + s_CORONA + "&sprache=" + s_SPRACHE
    + "&weiteresprachen=" + (s_SPRACHE == "0" ? s_WEITERESPRACHEN : "") + "&beszg=" + s_VOLLTEXT
'''
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .models import PtkBayern, Base
from itemadapter import ItemAdapter

# class DocscraperPipeline:
#     def process_item(self, item, spider):
#         return item

class SQLAlchemyPipeline(object):
    def open_spider(self, spider):
        engine = create_engine('sqlite:///ptk_bayern.db')
        Base.metadata.create_all(engine)
        self.Session = sessionmaker(bind=engine)

    def close_spider(self, spider):
        self.Session.close_all()

    def process_item(self, item, spider):
        session = self.Session()
        adapter = ItemAdapter(item)  # create an adapter for the item

        # convert values to appropriate types
        if adapter.get('postal_code'):
            adapter['postal_code'] = int(adapter['postal_code'])
        if adapter.get('score'):
            adapter['score'] = float(adapter['score'])

        ptk_bayern = PtkBayern(**dict(adapter))  # convert adapter back to dict

        try:
            session.add(ptk_bayern)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

        return item

import scrapy
import json

from docscraper.items import PtkBayernItem
from urllib.parse import urlencode


class PtkBayernSpider(scrapy.Spider):
    name = "ptk_bayern"
    allowed_domains = ["ptk-bayern.de"]

    def start_requests(self):
        base_url = "https://www.ptk-bayern.de/ptk/adressen.nsf/ptk_search_psychotherapeuten?OpenAgent"
        params = {
            "plz": "",
            "ort": "",
            "umkreis": "0",
            "name": "",
            "pgruppe": "er",
            "versicherung": "",
            "wleistungen": "",
            "verfahren": "",
            "weitereverfahren": "300",
            "weitereverfahren_wissen": "undefined",
            "geschlecht": "",
            "oeffentlich": "",
            "barrierefrei": "",
            "corona": "undefined",
            "sprache": "",
            "weiteresprachen": "",
            "beszg": "",
        }

        url = base_url + "&" + urlencode(params)
        print(url)
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        print(response.text)
        data = json.loads(response.text)
        for item in data["data"]:
            yield PtkBayernItem(
                salutation=item["anrede"],
                services=item["dienstleistungen"],
                email=item["email"],
                company=item["firma"],
                geoCoordinates=item["geoCoordinates"],
                homepage=item["homepage"],
                country=item["land"],
                location=item["ort"],
                patient_group=item["patientengruppe"],
                postal_code=item["plz"],
                score=item["score"],
                last_name=item["sname"],
                street=item["strasse"],
                first_name=item["svorname"],
                fax=item["telefax"],
                telephone=item["telefon"],
                url=item["url"],
                billing_basis=item["verrechnungsbasis"],
            )

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class PtkBayern(Base):
    __tablename__ = 'ptk_bayern'

    id = Column(Integer, primary_key=True)
    salutation = Column(String)
    services = Column(String)
    email = Column(String)
    company = Column(String)
    geoCoordinates = Column(String)
    homepage = Column(String)
    country = Column(String)
    location = Column(String)
    patient_group = Column(String)
    postal_code = Column(Integer)
    score = Column(String)
    last_name = Column(String)
    street = Column(String)
    first_name = Column(String)
    fax = Column(String)
    telephone = Column(String)
    url = Column(String)
    billing_basis = Column(String)
